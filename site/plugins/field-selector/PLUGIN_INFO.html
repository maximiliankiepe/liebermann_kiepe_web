<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
</head>
<body>
<p>This additional panel field for <a href="http://getkirby.com">Kirby 2</a> allows you to use an intuitive alternative file selection field in your blueprints.</p>

<p><strong>Authors</strong>: <a href="https://github.com/storypioneers">digital storytelling pioneers</a> feat. <a href="https://github.com/JonasDoebertin">Jonas Doebertin</a></p>

<p><strong>License</strong>: <a href="http://opensource.org/licenses/GPL-3.0">GNU GPL v3.0</a></p>

<figure>
<img src="https://raw.githubusercontent.com/storypioneers/kirby-selector/master/screenshot.png" alt="Screenshot" />
<figcaption>Screenshot</figcaption>
</figure>

<h2 id="installation">Installation</h2>

<p>Copy or link the <code>selector</code> directory to <code>site/plugins/</code> <strong>or</strong> use the <a href="https://github.com/getkirby/cli">Kirby CLI</a> <code>kirby plugin:install storypioneers/kirby-selector</code>.</p>

<h2 id="usage">Usage</h2>

<h3 id="withinyourblueprints">Within your blueprints</h3>

<p>Post installation simply add <code>selector</code> fields to your blueprints and set some options (where applicable).</p>

<pre><code class="yaml">fields:
    postimage:
        label: Main Post Image
        type:  selector
        mode:  single
        types:
            - image
</code></pre>

<pre><code class="yaml">fields:
    attachments:
        label: Post Attachments
        type:  selector
        mode:  multiple
        types:
            - all
</code></pre>

<h3 id="withinyourtemplatescontrollersmodels">Within your templates / controllers / models</h3>

<p>As per design, the selector field stores the <em>filenames</em> of the selected files, only. If you need access to the files full path or to other properties / functions of the file object, you must convert the filename into a full file object first.</p>

<p><strong>Single Mode</strong></p>

<p>When you&#8217;re using the Selector field in Single Mode, gaining access to the full file object is quite easy. Just replace <code>yourselectorfield</code> with the name of your Selector-based field.</p>

<pre><code class="php">    // Convert the filename to a full file object
    $file = $page-&gt;yourselectorfield()-&gt;toFile();

    // Use the file object
    echo $file-&gt;url();
</code></pre>

<p><strong>Multiple Mode</strong></p>

<p>In multiple mode, the Selector field stores a comma-separated list of filenames, based on how many files you selected. To convert this list into a fully-featured file collection (just like <code>$page-&gt;files()</code>), you need a bit more code.</p>

<pre><code class="php">
    // Transform the comma-separated list of filenames into a file collection
    $filenames = $page-&gt;yourselectorfield()-&gt;split(',');
    if(count($filenames) &lt; 2) $filenames = array_pad($filenames, 2, '');
    $files = call_user_func_array(array($page-&gt;files(), 'find'), $filenames);

    // Use the file collection
    foreach($files as $file)
    {
        echo $file-&gt;url();
    }

</code></pre>

<h2 id="options">Options</h2>

<p>The field offers some options that can be set on a per field basis directly from your blueprints.</p>

<h3 id="mode">mode</h3>

<p>Define a mode the field will work in. Possible values are <code>single</code> and <code>multiple</code>.</p>

<ul>
<li><p><strong>single</strong>: With the <code>single</code> mode, the fields checkboxes will work like a set of radio buttons, letting you select a single file only. This is useful if you want a way to specify a posts main image.</p></li>
<li><p><strong>multiple</strong>: This option allows you to select multiple files in a single file selector field. The selected files will be stored as a comma separated list.</p></li>
</ul>

<h3 id="types">types</h3>

<p>Define the files types the file selector field shows. Possible values are <code>all</code>, <code>image</code>, <code>video</code>, <code>audio</code>, <code>document</code>, <code>archive</code>, <code>code</code>, <code>unknown</code>. You may specify as many of these types as you like.</p>

<pre><code class="yaml">fields:
    attachments:
        label: Post Attachments
        type:  selector
        mode:  multiple
        types:
            - image
            - video
            - audio
            - document
</code></pre>

<h3 id="sort">sort</h3>

<p>Files will be shown sorted by their filename in ascending order (a-z). However, this option let&#8217;s you change the default sort behavior. You can sort files by filesize, dimensions, type and many more. Some of the countless possible values are <code>sort</code>, <code>filename</code>, <code>size</code>, <code>width</code>, <code>height</code>, <code>type</code>, <code>modified</code>, <code>ratio</code>. You can also sort by any of your custom <a href="http://getkirby.com/docs/panel/blueprints/file-settings#file-fields">file fields</a>. The value <code>sort</code> will make sure the files are presented in the exact order you specified in the panels file section via <a href="http://getkirby.com/docs/panel/blueprints/file-settings#sortable-files">drag and drop</a>.</p>

<pre><code class="yaml">fields:
    featured:
        label: Featured Image
        type:  selector
        mode:  single
        sort:  size
        types:
            - image
</code></pre>

<h3 id="flip">flip</h3>

<p>This options allows you to reverse the sort order you specified with the <code>sort</code> option. You may set this to <code>true</code> or <code>false</code>.</p>

<h3 id="autoselect">autoselect</h3>

<p>This options allows you to tell the Selector to auto select the first, last or all files of the list, if no other file is selected, yet. Possible values are <code>none</code> (default), <code>first</code>, <code>last</code> and <code>all</code>.</p>

<pre><code class="yaml">fields:
    featured:
        label:      Featured Image
        type:       selector
        mode:       single
        sort:       filename
        autoselect: first
        types:
            - image
</code></pre>

<h3 id="filter">filter</h3>

<p>This options allows you to set a filename filter. This can be either a simple string or a fully featured regular expression. Only files with filenames matching the filter string or regular expression will be shown in the Selector field. You may set this to any string like <code>background</code>, <code>.min.js</code> or <code>large</code> or a regular expression like <code>/\.((png)|(jpe?g))/i</code>.</p>

<pre><code class="yaml">fields:
    featured:
        label:  Page Background Image
        type:   selector
        mode:   single
        filter: background
        types:
            - image
</code></pre>

<p>Showing only image files with the term <em>background</em> in their filename.</p>

<pre><code class="yaml">fields:
    featured:
        label:  Page Background Image
        type:   selector
        mode:   single
        filter: /\.((png)|(jpe?g))/i
</code></pre>

<p>Using a regular expression to show only <em>.jpg</em>, <em>.jpeg</em> and <em>.png</em> files.</p>

<h3 id="size">size</h3>

<p>The <code>size</code> option lets you limit the height of the selector field and makes it scrollable. Only the specified number of files will be shown initially. This can be set either to a <em>number</em> or to <code>auto</code> (the Selector field will adapt to the height of the complete list of files).</p>

<pre><code class="yaml">fields:
    featured:
        label: Page Background Image
        type:  selector
        mode:  single
        size:  4
</code></pre>

</body>
</html>
