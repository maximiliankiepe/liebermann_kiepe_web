<!-- HEAD + MENU -->
<?php snippet('header') ?>

<!-- SMALL TEASER -->
<div class="barba-container" data-namespace="work">
  <canvas id="canvas" class="canvasClass" resize></canvas>
  <div class="workingclass">
    <?php snippet('smallteaser') ?>
  </div>
</div>
<!-- FOOTER -->
<?php snippet('footer') ?>
