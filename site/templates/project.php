<!-- HEAD + MENU -->
<?php snippet('header') ?>

<div class="barba-container" data-namespace="project1">

<?php if($page->hasTemplate('news')): ?>

    <div class="projectcontentinner work">

      <?php if($page->hasChildren()): ?>
        <?php $detect = new Mobile_Detect; if (!$detect->isMobile()): ?>
          <div id="vid1" class="videowrap" data-url1="<?php echo $page->url1()->html() ?>" >
            <video loop muted class="image1" id="bgvid" poster="<?php if ($page->mainimage()->isNotEmpty()): ?><?php echo $page->image($page->mainimage())->url() ?><?php endif ?>">
              <?php foreach($page->children()->videos()->filterBy('extension', 'mp4') as $video): ?>
                <source src="<?php echo $video->url() ?>" type="video/mp4">
              <?php endforeach ?>
              <?php foreach($page->children()->videos()->filterBy('extension', 'webm') as $video): ?>
                <source src="<?php echo $video->url() ?>" type="video/webm">>
              <?php endforeach ?>

            </video>
          </div>
        <?php endif ?>
        <?php $detect = new Mobile_Detect; if ($detect->isMobile()): ?>
          <?php if ($page->mainimage()->isNotEmpty()): ?>
              <div class="image mob" style="background-image: url(<?php echo $page->image($page->mainimage())->url() ?>)"></div>
          <?php endif ?>
        <?php endif ?>
      <?php endif ?>

      <div class="teasertitle visit mactive">
        <?php if($page->project()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Project:</h1><h2><?php echo $page->project()->text() ?></h2>
          </div>
        <?php endif ?>
        <?php if($page->client()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Client:</h1><h2><?php echo $page->client()->text() ?></h2>
          </div>
        <?php endif ?>
        <?php if($page->year()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Year:</h1><h2><?php echo $page->year()->text() ?></h2>
          </div>
        <?php endif ?>
        <?php if($page->type()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Type:</h1><h2><?php echo $page->type()->text() ?></h2>
          </div>
        <?php endif ?>
        <?php if($page->service()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Service:</h1><h2><?php echo $page->service()->text() ?></h2>
          </div>
        <?php endif ?>
        <?php if($page->contributors()->isNotEmpty()): ?>
          <div class="teasertitleinner">
            <h1>Colaborators:</h1><h2><?php echo $page->contributors()->text() ?></h2>
          </div>
        <?php endif ?>
      </div>

      <?php if ($page->postimage()->isNotEmpty()): ?>
        <div class="image">

          <canvas id="canvas" class="canvasClassProject" resize></canvas>

          <?php if ($page->backgroundimage()->isNotEmpty()): ?>
            <div class="imagecontent blur" style="background-image: url(<?php echo thumb($page->image($page->backgroundimage()), array('width' => 800, 'blur' => true))->url() ?>)"></div>
          <?php endif ?>

          <div class="imagecontent_overlay">

          <?php $filenames = $page->postimage()->sortBy('sort', 'asc')->split(','); ?>
          <?php if(count($filenames) < 2) $filenames = array_pad($filenames, 2, ''); ?>
          <?php $files = call_user_func_array(array($page->files()->sortBy('sort', 'asc'), 'find'), $filenames); ?>
          <?php $n=0; foreach($files->sortBy('sort', 'asc') as $file): $n++; ?>

            <?php if($file->type() == 'image'): ?>
              <figure>
                <img class="image<?php echo $n ?>" src="<?php echo kirby()->urls()->assets() ?>/img/bg.png" data-src="<?php echo thumb($file, array('width' => 1200))->url() ?>" alt="<?php echo $page->title()->html() ?>">
              </figure>
            <?php endif ?>
            <?php if($file->type() == 'video'): ?>
              <figure>
                <video class="image<?php echo $n ?>" loop muted src="<?php echo kirby()->urls()->assets() ?>/img/bg.png" data-src="<?php echo $file->url() ?>"/>
              </figure>
            <?php endif ?>
          <?php endforeach ?>
          </div>
        <?php endif ?>

      </div>

      <?php if($page->clientdescription()->isNotEmpty()): ?>
        <div class="article c">
          <h1>
            Costumer:
          </h1>
          <p>
            <p class="text full">
            <?php echo $page->clientdescription()->text() ?>
          </p>
        </div>
      <?php endif ?>

        <?php if($page->graphicdescription()->isNotEmpty()): ?>
          <div class="article vc">
            <h1>
              Visual concept:
            </h1>
            <p>
              <p class="text full">
              <?php echo $page->graphicdescription()->text() ?>
            </p>
          </div>
        <?php endif ?>

        <?php if($page->techdescription()->isNotEmpty()): ?>
          <div class="article ic">
            <h1>
              Interaction Concept:
            </h1>
            <p>
              <p class="text full">
              <?php echo $page->techdescription()->text() ?>
            </p>
          </div>
        <?php endif ?>
<?php endif ?>

<div class="smallteaser">
	<?php foreach($page->siblings($self = false) as $project): ?>
		<?php if(($project->hasTemplate('news'))): ?>
			<a href="<?php echo $project->url() ?>" class="teasertitle">
		      <?php if($project->project()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Project:</h1><h2><?php echo $project->project()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->client()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Client:</h1><h2><?php echo $project->client()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->year()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Year:</h1><h2><?php echo $project->year()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->type()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Type:</h1><h2><?php echo $project->type()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->service()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Service:</h1><h2><?php echo $project->service()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->contributors()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Colaborators:</h1><h2><?php echo $project->contributors()->text() ?></h2>
		        </div>
		      <?php endif ?>
			</a>
		<?php endif ?>
	<?php endforeach ?>
  <div>


<!-- FOOTER -->
<?php snippet('footer') ?>
