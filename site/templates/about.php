<!-- HEAD + MENU -->
<?php snippet('header') ?>

<div class="barba-container" data-namespace="about">

  <canvas id="canvas" class="canvasClass" resize></canvas>

  <div class="projectcontentinner">

    <?php if($page->about()->isNotEmpty()): ?>
      <div class="article">
          <?php echo $page->about()->kirbytext() ?>
      </div>
    <?php endif ?>

    <?php snippet('stories') ?>

    <?php foreach($pages->find("about")->images() as $image): ?>
      <div class="aboutimagecontent" style="background-image: url(<?php echo $image->url() ?>)"></div>
    <?php endforeach ?>

    <?php if($page->manifest()->isNotEmpty()): ?>
      <div class="article manifest">
          <?php echo $page->manifest()->kirbytext() ?>
      </div>
    <?php endif ?>

    <?php if($page->clients()->isNotEmpty()): ?>
      <div class="article">
          <?php echo $page->clients()->kirbytext() ?>
      </div>
    <?php endif ?>

    <?php foreach($page->children() as $project): ?>
          <div class="social mactive">
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
            <a href="<?php echo $project->link()->text() ?>"><?php echo $project->channel()->text() ?></a>
          </div>
    <?php endforeach ?>

    <?php if($pages->find('imprint')->special()->isNotEmpty()): ?>
      <div class="article imprint">
        <?php echo $pages->find('imprint')->special()->kirbytext() ?>
      </div>
    <?php endif ?>

    <?php if($pages->find('imprint')->title()->isNotEmpty()): ?>
          <div class="social mactive imprint_cta">
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
            <div class="imp"><?php echo $pages->find('imprint')->title()->text() ?></div>
          </div>
    <?php endif ?>


    <?php if($pages->find('imprint')->imprint()->isNotEmpty()): ?>
      <div class="article imprint a closed">
        <?php echo $pages->find('imprint')->imprint()->kirbytext() ?>
      </div>
    <?php endif ?>


  <!-- </div> -->

<!-- FOOTER -->
<?php snippet('footer') ?>
