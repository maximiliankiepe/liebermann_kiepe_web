
<!-- HEAD + MENU -->
<?php snippet('header') ?>


<!-- BIG TEASER + NEWS -->
<div class="barba-container" data-namespace="homepage">
<canvas id="canvas" class="canvasClass" resize></canvas>
<?php snippet('bigteaser') ?>
</div>
<!-- FOOTER -->
<?php snippet('footer') ?>
