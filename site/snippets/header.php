<!DOCTYPE html>

<!--

 /$$         /$$   /$$
| $$        | $$  /$$/
| $$        | $$ /$$/
| $$        | $$$$$/
| $$        | $$  $$
| $$        | $$\  $$
| $$$$$$$$  | $$ \  $$
|________/  |__/  \__/

date: 05-01-2018

-->

<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Description -->
  <meta name="description" content="<?= $site->description()->html() ?>">

  <!-- SEO/All Search Engines -->
  <meta name="robots" content="index,follow,noodp">
  <!-- SEO/Google Specific -->
  <meta name="googlebot" content="index,follow">
  <meta name="google" content="notranslate">
  <meta name="classification" content="business">
  <meta name="url" content="<?= $site->url() ?>">

  <!-- Geo tags -->
  <meta name="ICBM" content="53.5366297,10.0273006">
  <meta name="geo.position" content="53.5366297,10.0273006">
  <meta name="geo.placename" content="Hamburg">

  <!-- Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?= $site->title()->html() ?>">
  <meta property="og:image" content="<?= $site->cover_image()->url() ?>">
  <meta property="og:description" content="<?= $site->description()->html() ?>">
  <meta property="og:site_name" content="<?= $site->title()->html() ?>">
  <meta property="og:locale" content="de_DE">
  <meta property="article:author" content="David Liebermann, Maximilian Kiepe">

  <!-- Twitter -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@liebermannkiepe">
  <meta name="twitter:creator" content="@liebermannkiepe">
  <meta name="twitter:title" content="<?= $site->description()->html() ?>">
  <meta name="twitter:description" content="<?= $site->description()->html() ?>">
  <meta name="twitter:image" content="<?= $site->cover_image()->url() ?>">

  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo kirby()->urls()->assets() ?>/css/main.css"/>
  <!-- <link rel="stylesheet" type="text/css" href="<?php echo kirby()->urls()->assets() ?>/css/nprogress.css"/> -->

  <!-- Header JS -->
  <script type="text/javascript" src="<?php echo kirby()->urls()->assets() ?>/js/paper.min.js"></script>
  <script type="text/javascript" src="<?php echo kirby()->urls()->assets() ?>/js/instafeed.js"></script>

  <title><?= $site->title()->html() ?></title>

</head>
<body>

<header>
  <?php snippet('menu') ?>
</header>

<main id="barba-wrapper">

  <div id="businesscard">
    <span class="front">
      <div class="businessinner">
        <div class="a1">
          <strong>Liebermann Kiepe</strong><br>
          <p>Digital Design Studio</p>
        </div>
      </div>
    </span>
    <span class="back">
      <div class="businessinner">
        <div class="b1">
          <p>Brandshofer Deich 45</p><br>
          <p>D – 20539 Hamburg</p>
        </div>
        <div class="b2">
          <p>+ 49 (0) 176 3126 5047</p><br>
          <p>info@liebermannkiepe.de</p>
        </div>

      </div>
    </span>
  </div>

  <a id="businesscardfake" href="mailto:info@liebermannkiepe.de">
    <span class="front">
    </span>
    <span class="back">
    </span>
  </a>
