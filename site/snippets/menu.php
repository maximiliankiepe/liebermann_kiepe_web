<menu>
	<h1 id="home_cta" class="navitem">
	<?php if( $site->language()->code() == 'de'): ?>
		<a href="<?php echo $site->language('de')->url() ?>" ><?= $pages->find('home')->pagetitle()->html() ?></a>
	<?php elseif( $site->language()->code() == 'en'): ?>
		<a href="<?php echo $site->language('en')->url() ?>" ><?= $pages->find('home')->pagetitle()->html() ?></a>
	<?php endif ?>
	</h1><img class="cross" src="<?php echo kirby()->urls()->assets() ?>/img/x.png"><h2 id="work_cta" class="navitem"
	>
		<?php $detect = new Mobile_Detect; if (!$detect->isMobile()): ?><a href="<?php echo $pages->find("work")->url() ?>"><?= $pages->find('home')->nav1()->html() ?></a>
	</h2><?php endif ?><?php $detect = new Mobile_Detect; if ($detect->isMobile()): ?><a class="mactive" href="<?php echo $pages->find("work")->url() ?>"><div class="teasertitleinner"><?= $pages->find('home')->nav1()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav1()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav1()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav1()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav1()->html() ?></div></a>
</h2><?php endif ?><h2 id="about_cta" class="navitem"
	>
		<?php $detect = new Mobile_Detect; if (!$detect->isMobile()): ?><a href="<?php echo $pages->find("about")->url() ?>"><?= $pages->find('home')->nav2()->html() ?></a>
	</h2><?php endif ?><?php $detect = new Mobile_Detect; if ($detect->isMobile()): ?><a class="mactive" href="<?php echo $pages->find("about")->url() ?>"><div class="teasertitleinner"><?= $pages->find('home')->nav2()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav2()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav2()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav2()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav2()->html() ?></div></a>
</h2><?php endif ?><?php $detect = new Mobile_Detect; if (!$detect->isMobile()): ?><h2 id="contact_cta" class="navitem"
	>
		<a ><?= $pages->find('home')->nav3()->html() ?></a>
	</h2><?php endif ?><?php $detect = new Mobile_Detect; if ($detect->isMobile()): ?><a id="contact_cta" class="navitem mactive"><div class="teasertitleinner"><?= $pages->find('home')->nav3()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav3()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav3()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav3()->html() ?></div><div class="teasertitleinner"><?= $pages->find('home')->nav3()->html() ?></div></a>
</h2><?php endif ?>

    <?php if( $site->language()->code() == 'de'): ?>
		<h2 class="navitem language">
			<a class="no-barba" href="<?= $site->language('en')->url() ?>">
				En
			</a>
		</h2>
	<?php elseif( $site->language()->code() == 'en'): ?>
		<h2 class="navitem language">
			<a class="no-barba" href="<?= $site->language('de')->url() ?>">
				De
			</a>
		<h2>
	<?php endif ?>
</menu>
