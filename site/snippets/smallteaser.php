<div class="smallteaser">
	<?php foreach(page('home')->children()->visible() as $project): ?>
		<?php if(($project->hasTemplate('news'))): ?>
			<a href="<?php echo $project->url() ?>" data-url1="<?php echo $project->url1()->html() ?>" class="teasertitlework">
		      <?php if($project->project()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Project:</h1><h2><?php echo $project->project()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->client()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Client:</h1><h2><?php echo $project->client()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->year()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Year:</h1><h2><?php echo $project->year()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->type()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Type:</h1><h2><?php echo $project->type()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->service()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Service:</h1><h2><?php echo $project->service()->text() ?></h2>
		        </div>
		      <?php endif ?>
		      <?php if($project->contributors()->isNotEmpty()): ?>
		        <div class="teasertitleinner">
		          <h1>Collaborators:</h1><h2><?php echo $project->contributors()->text() ?></h2>
		        </div>
		      <?php endif ?>
			</a>
		<?php endif ?>
	<?php endforeach ?>
</div>
