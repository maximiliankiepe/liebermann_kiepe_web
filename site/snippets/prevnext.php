
<?php $collection = $page->siblings()->filterBy('template', 'project'); ?>

<div class="teasertitle break"><div class="teasertitleinner2">More Projects</div><div class="teasertitleinner2">More Projects</div><div class="teasertitleinner2">More Projects</div></div>

<nav>
  <ul class="prevNext">
    <?php if($prev = $page->getPrev($collection)): ?>
        <a href="<?= $prev->url() ?>"><li style="background-image: url(<?php echo $prev->image($prev->mainimage())->url() ?>)" class="previous"></li></a>
    <?php else: ?>
        <a href="<?= $prev = $page->parents()->children($collection)->last()->url(); ?>"><li style="background-image: url(<?= $prev = $page->parents()->children($collection)->last()->image($page->parents()->children($collection)->last()->mainimage())->url(); ?>)" class="previous"></li></a>
    <?php endif ?>


    <?php if($next = $page->getNext($collection)): ?>
        <a href="<?= $next->url() ?>"><li style="background-image: url(<?php echo $next->image($next->mainimage())->url() ?>)" class="previous next"></li></a>
    <?php else: ?>
        <a href="<?= $next = $page->parents()->children($collection)->first()->url(); ?>"><li style="background-image: url(<?= $next = $page->parents()->children($collection)->first()->image($page->parents()->children($collection)->first()->mainimage())->url(); ?>)" class="previous next"></li></a>
    <?php endif ?>
  </ul>
</nav>