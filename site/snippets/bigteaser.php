<?php $n=0; foreach(page('home')->children()->visible() as $project): $n++; ?>
	<?php if(($project->checkbox() == '1') && ($project->hasTemplate('news'))): ?>
		<a class="teaserwrap" data-url1="<?php echo $project->url1()->html() ?>" id="videowrap<?php echo $n ?>" href="<?php echo $project->url() ?>">

  			<?php if($project->hasChildren()): ?>
					<?php $detect = new Mobile_Detect; if (!$detect->isMobile()): ?>
						<div class="videowrap">
		  					<video muted autoplay loop class="image<?php echo $n ?>" id="bgvid" src="<?php echo kirby()->urls()->assets() ?>/img/bg.png" <?php foreach($project->children()->videos()->filterBy('extension', 'mp4') as $video): ?> data-src="<?php echo $video->url() ?>" type="video/mp4" <?php endforeach ?> poster="<?php if ($project->mainimage()->isNotEmpty()): ?><?php echo $project->image($project->mainimage())->url() ?><?php endif ?>">
		  					</video>
		  			</div>
					<?php endif ?>
					<?php $detect = new Mobile_Detect; if ($detect->isMobile()): ?>
			      <?php if ($project->mainimage()->isNotEmpty()): ?>
								<div class="image mob" style="background-image: url(<?php echo $project->image($project->mainimage())->url() ?>)"></div>
		        <?php endif ?>
					<?php endif ?>
  			<?php endif ?>


			<div class="teasertitle">
				<?php if($project->project()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Project:</h1><h2><?php echo $project->project()->text() ?></h2>
					</div>
				<?php endif ?>
				<?php if($project->client()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Client:</h1><h2><?php echo $project->client()->text() ?></h2>
					</div>
				<?php endif ?>
				<?php if($project->year()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Year:</h1><h2><?php echo $project->year()->text() ?></h2>
					</div>
				<?php endif ?>
				<?php if($project->type()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Type:</h1><h2><?php echo $project->type()->text() ?></h2>
					</div>
				<?php endif ?>
				<?php if($project->service()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Service:</h1><h2><?php echo $project->service()->text() ?></h2>
					</div>
				<?php endif ?>
				<?php if($project->contributors()->isNotEmpty()): ?>
					<div class="teasertitleinner">
						<h1>Colaborators:</h1><h2><?php echo $project->contributors()->text() ?></h2>
					</div>
				<?php endif ?>
			</div>
		</a>
	<?php endif ?>
	<?php if(!$project->hasTemplate('news')): ?>
			<div class="special">
				<?php echo $project->news()->kirbytext() ?>
			</div>
	<?php endif ?>
<?php endforeach ?>
