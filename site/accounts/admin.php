<?php if(!defined('KIRBY')) exit ?>

username: admin
password: >
  $2a$10$vu9vnMsqeFKGOa3kcU/z6uTVxUpmmDyJryxM6xuDu81aj0.fmgnei
email: hallo@davidliebermann.de
language: en
role: admin
history:
  - home/project-small
  - home/villa-huegel-interaction
  - home/kunsthalle-rostock
  - home/project2-small
  - home/project2
