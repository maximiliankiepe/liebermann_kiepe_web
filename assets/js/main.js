// Card Flip
var switched = 'true';
var sw_b = 'true';
var x = $(window).width() / 2 + (Math.random() * (-50 - 50));
var y = $(window).height() / 2 + (Math.random() * (-50 - 50));
var angle = (Math.random() * (0-360));
var scrollpos = 0;

$(window).scroll(function(){
	scrollpos = $(document).scrollTop();
})

$(window).on('load', function(){
	var vw = ($(window).width() / 100) * 32;
	$('.scontainer').css({'width': vw});
	$('.scontainer').css({'height': vw});
	$('.scontainer').fadeIn("fast");
});

// DOCUMENT READY
$(document).on('ready', function() {

	init();

	$('#canvas').hide();

	// INSTAGRAM VIDEO PLAY ON HOVER
	$(document).on('mouseenter', '.scontainer > video',  function(){
		$(this).get(0).play();
	});

	$(document).on('mouseleave', '.scontainer > video',  function(){
			$(this).get(0).pause();
	});

	// HOVER OVER LOGO
	$('#home_cta a').mouseover(function(){
		$(this).html('Selected Works');
	})
	$('#home_cta a').mouseout(function(){
		$(this).html('Liebermann Kiepe');
	})

	// BUSINESSCARDS IN AND OUT
	function cardIn(){
		x = $(window).width() / 2 + (Math.random() * (-50 - 50));
		y = $(window).height() / 2 + (Math.random() * (-50 - 50));
		angle = (Math.random() * (0-360));
		if (sw_b == 'true'){
			$('#businesscard, #businesscardfake').velocity({
				top:  scrollpos+y+'px',
				left: x+'px',
				rotateZ: angle+"deg",
			},
			{
				duration:600,
				easing:"easeOutQuad"
			})
			sw_b = 'false';
		}
	};

	function cardOut(){
		angle = (Math.random() * (0-360));
		if (sw_b == 'false'){
			$('#businesscard, #businesscardfake').velocity({
				top:  '-50vh',
				rotateZ: angle+"deg",
			},
			{
				duration:600,
				easing:"easeOutQuad"
			})
			sw_b = 'true';
		}
	};

	$('#contact_cta').on( "click", function() {
		if ( sw_b == 'true' ){
			cardIn();
		} else {
			cardOut();
		}
	});

	$("menu a:not('#contact_cta a')").on( "click", function() {
			cardOut();
	});

	// INIT BARBA
	Barba.Pjax.start();

	// DO FUNCTIONS
	mobilemenu();
	// videoplaypause();
	imprint_cta()

});

// BUSINESSCARS FLIP FUNCTION
function business(){

	$('#businesscardfake').mouseenter(function(){
		$('#businesscard').addClass('blue');
		x = (Math.random() * (-50 - 50));
		y = (Math.random() * (-50 - 50));
		angle = (Math.random() * (120-320));
		$btn = $('#businesscard');
		$btnfake = $('#businesscardfake');
		if ( switched == 'true' ){
			$btn.velocity({
		  	translateY: y+"px",
		  	translateX: x+"px",
		  	rotateY: "90deg",
		  	rotateZ: angle+"deg",
		  	scale:1.5
			},
			{
		  	duration:300,
		  	easing:"easeInQuad",
		  	complete:function(){
		    	$('.front').css('display','none');
		    	$('.back').css('display','block');
		  	}
			})
			.velocity({
		  	translateY: y+"px",
		  	translateX: x+"px",
		  	rotateY: "0deg",
		  	rotateZ: angle+"deg",
		  	scale:1
			},
			{
		  	duration:300,
		  	easing:"easeOutQuad"
			})
			$btnfake.velocity({
		  	translateY: y+"px",
		  	translateX: x+"px",
		  	rotateZ: angle+"deg",
		  	scale:1.5
			},
			{
		  	duration:300,
		  	easing:"easeInQuad",
		  	complete:function(){
		    	$('.front').css('display','none');
		    	$('.back').css('display','block');
		  	}
			})
			.velocity({
		  	translateY: y+"px",
		  	translateX: x+"px",
		  	rotateZ: angle+"deg",
		  	scale:1
			},
			{
		  	duration:300,
		  	easing:"easeOutQuad",
		  	complete:function(){
		    	switched = 'false';
		  	}
			})
		} else if ( switched == 'false'){
			$btn.velocity({
			  translateY: y+"px",
			  translateX: x+"px",
			  rotateX: "90deg",
			  rotateZ: angle+"deg",
			  scale:1.5
			},
			{
			  duration:300,
			  easing:"easeInQuad",
			  complete:function(){
			    $('.front').css('display','block');
			    $('.back').css('display','none');
			  }
			})
			.velocity({
			   translateY: y+"px",
			   translateX: x+"px",
			   rotateX: "0deg",
			   rotateZ: angle+"deg",
			   scale:1
			 },
			 {
			   duration:300,
			   easing:"easeOutQuad"
			 })
			 $btnfake.velocity({
			   translateY: y+"px",
			   translateX: x+"px",
			   rotateZ: angle+"deg",
			   scale:1.5
			 },
			 {
			   duration:300,
			   easing:"easeInQuad",
			   complete:function(){
			     $('.front').css('display','block');
			     $('.back').css('display','none');
			   }
			 })
			 .velocity({
			   translateY: y+"px",
			   translateX: x+"px",
			   rotateZ: angle+"deg",
			   scale:1
			 },
			 {
			   duration:300,
			   easing:"easeOutQuad",
			   complete:function(){
			     switched = 'true';
			   }
			 })
			}
	})
	$('#businesscardfake').mouseleave(function(){
		$('#businesscard').removeClass('blue');
	});
}

// BARBA TRANSITIONS
Barba.Pjax.init();
Barba.Prefetch.init();
Barba.Utils.xhrTimeout = 200;

var lastElementClicked;
var urls = [];

var Homepage = Barba.BaseView.extend({
  namespace: 'homepage',
  onEnter: function() {
      // The new Container is ready and attached to the DOM
			$('#canvas').hide();
			videoheight();
			tool2.activate();
			imageload();
			isInVi();
  },
  onEnterCompleted: function() {
      // The Transition has just finished.
  },
  onLeave: function() {
      // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  }
});

var About = Barba.BaseView.extend({
  namespace: 'about',
  onEnter: function() {

		insta();
    // The new Container is ready and attached to the DOM
  },
  onEnterCompleted: function() {
      // The Transition has just finished.
  },
  onLeave: function() {

		$('#canvas').hide();
		tool2.activate();

      // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  }
});

var Work = Barba.BaseView.extend({
  namespace: 'work',
  onEnter: function() {
      // The new Container is ready and attached to the DOM
  },
  onEnterCompleted: function() {
      // The Transition has just finished.
  },
  onLeave: function() {
		tool2.activate();
      // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  }
});

var Project1 = Barba.BaseView.extend({
  namespace: 'project1',
  onEnter: function() {
		tool1.activate();
		word = document.getElementById('vid1').dataset.url1;
		tool1.activate();
		$('#canvas').show();
		paper.view.detach('frame', view.onFrame);
		project_scrolledtop();
		videoheight();
		imageload();
		isInVi();
      // The new Container is ready and attached to the DOM
  },
  onEnterCompleted: function() {
      // The Transition has just finished.
  },
  onLeave: function() {
			$('#canvas').hide();
			tool2.activate();
      // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  }
});

// Don't forget to init the view!
$(window).load(function(){
	// Homepage.init();
	// Project1.init();
});

Work.init();
About.init();
Project1.init();
Homepage.init();

// NEW PAGE READY
Barba.Dispatcher.on('newPageReady', function(current, prev, container) {
    history.scrollRestoration = 'manual';
		isInVi();
		project_scrolledtop();
		$('a:visited').addClass('visitor');
		$('.image').addClass('start');
		imageload();
});

// LINK CLICKED
Barba.Dispatcher.on('linkClicked', function(el) {
	lastElementClicked = el;
	if ( lastElementClicked.className == 'teasertitlework' ){
		word = lastElementClicked.dataset.url1;
		console.log(lastElementClicked.dataset.url1);
		tool2.activate();
		$('#canvas').show();
	}
	if ( lastElementClicked.className == 'teaserwrap' ){
		word = lastElementClicked.dataset.url1;
		console.log(lastElementClicked.dataset.url1);
		tool2.activate();
		$('#canvas').show();
	}
});

// TRANSITION COMPLETED
Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {

  // DO FUNCTIONS
  marquee();
  videoheight();
	imprint_cta()
  // videoplaypause();
	business();
	$('body, html, main, .barba-container, .projectcontentinner, projectcontent').scrollTop(0);
});

// THE TRANSITION
var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
		Promise
		.all([
			this.newContainerLoading,
			this.blurOut()
		])
		.then(
			this.blurIn.bind(this)
		);
  },
  blurOut: function() {
		var y = $(window).height() * 3.5 / 100;
		var u = $(window).height() / 100;
		var yu = y + y;
		return $(lastElementClicked).velocity("scroll", { duration: 500, offset: -yu, mobileHA: false }).promise();
  },
  blurIn: function() {
    var _this = this;
    var $el = $(this.newContainer);
    $(this.oldContainer).hide();
    $el.css({
      visibility : 'visible',
      opacity : 1
    });
    $el.animate({
			opacity: 1
		}, 0, function() {
      _this.done();
			var y = $(window).height() - $(window).height() * 12 / 100  ;
			$('.barba-container').velocity("scroll", { duration: 1000, offset: y });
    });
  }
});

// NORMAL HIDE SHOW TARNSITION
var HideShowTransition = Barba.BaseTransition.extend({
  start: function() {
    this.newContainerLoading.then(this.finish.bind(this));
  },
  finish: function() {
    this.done();
  }
});

// BRABA DEFINE WHICH TRANSITION BASED ON WHAT WAS CLICKED
Barba.Pjax.getTransition = function() {
	if ( lastElementClicked.className == 'teaserwrap' ){
		return FadeTransition;
	}
	else {
		return HideShowTransition;
	}

};

// OTHER FUNCTIONS

// RESPONSIVE HEIGHT
function videoheight(){
	console.log('on')
	var videocontainerheight = $(".videowrap").height();
	var videoheight = $("video").height();
	var winheight = $(window).height() / 100 * 3.5;
	var imgheight = $("figure > img").height();
	var figheight = imgheight + winheight;

	if(videoheight <= videocontainerheight){
		$(".videowrap").css("height", videoheight);
	}

	$(window).resize(function(){
		$('body, html, main, .barba-container, .projectcontentinner, projectcontent').scrollTop(0);

		var videocontainerheight = $(".videowrap").height();
		var videoheight = $("video").height();
		var imgheight = $("figure > img").height();
		var figheight = imgheight + winheight;

		$(".image, .imagecontent_overlay").css("height", imgheight);
		$("figure").css("minHeight", figheight);

		if(videoheight <= videocontainerheight){
			$(".videowrap").css("height", videoheight);
		}else{
			$(".videowrap").removeAttr("style");
		}
	});
};

// PROJECT SCROLLED TOP
function project_scrolledtop() {
		$('.imagecontent_overlay').on('scroll', function(){
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				$(".image").css({"pointerEvents": "none"});
				$('.image').addClass('end');
			} else if($(this).scrollTop() == 0) {
				$(".image").css({"pointerEvents": "none"});
				$('.image').addClass('start');
			}
		});

		$(document).bind('wheel mousewheel scroll touchscroll DOMMouseScroll', '.imagecontent_overlay', function(event) {

			var scrolled = $(window).scrollTop();
			var bottom = $('.teasertitle').offset().top;
	    if (event.originalEvent.detail < 0 || event.originalEvent.wheelDelta > 0) {
					if(scrolled <= bottom && !$('.image').hasClass('start')){
						$(".image").css({"pointerEvents": "auto"});
						$(".image").removeClass('end');
						var offset = $(window).width() / 100 * 3.5;
						$.extend($.scrollTo.defaults, {
							offset: {top: -offset},
						});
						$(window).scrollTo('.image');
					}
	    }
	    else {
				if(scrolled >= bottom && !$('.image').hasClass('end')){
					$(".image").css({"pointerEvents": "auto"});
					$(".image").removeClass('start');
					var offset = $(window).width() / 100 * 3.5;
					$.extend($.scrollTo.defaults, {
						offset: {top: -offset},
					});
					$(window).scrollTo('.image');
				}
	    }
		});
}

// IMPRINT CTA
function imprint_cta() {
	$('.imprint_cta').on('click', function(){
		$('.imprint.a').removeClass('closed');
		setTimeout(function(){
			var offset = $(window).width() / 100 * 7.5;
			$.extend($.scrollTo.defaults, {
				offset: {top: -offset},
			  duration: 800
			});
		  $(window).scrollTo('.imprint.a');
		}, 100);
	});
}

// MARQUEES
function marquee(){
	if ($(window).width() > 750){
		$('.special, .mactive, .teaserwrap > .teasertitle, .smallteaser > .teasertitle, .smallteaser > .teasertitlework').marquee({
			duration: 20000,
			delayBeforeStart: 0,
			direction: 'left',
			pauseOnHover: true,
			duplicated: true,
			startVisible: true
		});
		$('.teaserwrap > .teasertitle').not('.special').marquee( "pause" );
		$('.teaserwrap > .teasertitle').on('mouseenter', function() {
		    $( this ).marquee( "resume" );
		});
		$('.teaserwrap > .teasertitle').on('mouseleave', function() {
		    $( this ).marquee( "pause" );
		});
	} else {
		$('.special, .mactive, .teaserwrap > .teasertitle, .smallteaser > .teasertitle, .smallteaser > .teasertitlework').marquee({
			duration: 20000,
			delayBeforeStart: 0,
			gap: 0,
			direction: 'left',
			pauseOnHover: false,
			duplicated: true,
			startVisible: true
		});

		$('.teaserwrap > .teasertitle').marquee( "resume" );
	}
};

// MOBILEMENU
function mobilemenu(){
	$('menu').removeClass('menuheight');
	$('main').removeClass('maintop');
	$('.cross').on('click', function(){
		$('menu').toggleClass('menuheight');
		$('main').toggleClass('maintop');
	});
	$('*').on('scroll', function(){
		$('menu').removeClass('menuheight');
		$('main').removeClass('maintop');
	});
	$('.menuitem').on('click', function(){
		$('menu').removeClass('menuheight');
		$('main').removeClass('maintop');
	});

}

function insta(){
var feed = new Instafeed({
	get: 'user',
	limit: 3,
	userId: '7237328069',
	clientId: 'fc6fa997646646c088514e6d985a1586',
	accessToken: '7237328069.ba4c844.2b31b0c4d4ae43ceba1acadb6b210e63',
	template: '',
	after: function () {
		calc_insta_size();
	},
	resolution: 'standard_resolution',
	filter: function(image) {
		if (image.type == 'image') {
				$('#instafeed').append('<a class="scontainer" href="https://www.instagram.com/liebermannkiepe/" target="_blank"><img src="' + image.images.standard_resolution.url + '" /></a>');
		} else {
				$('#instafeed').append('<a href="https://www.instagram.com/liebermannkiepe/" class="scontainer" target="_blank"><video loop><source src="' + image.videos.standard_resolution.url + '" type="video/mp4"/></video></a>');
		}
		return true;
	}
});

feed.run();
};

function calc_insta_size(){
console.log('ö');
var vw = ($(window).width() / 100) * 32;
$('.scontainer').css({'width': vw});
$('.scontainer').css({'height': vw});
$('.scontainer').fadeIn("fast");
};


function imageload(){
	$(window).load(function(){
		var data = $('.image1').attr("data-src");
		var data2 = $('.image2').attr("data-src");
		$('.image1').attr("src", data);
		$('.image2').attr("src", data2);
	})

	$(document).on('scroll', function(){
		$('img, video').not('.inview').each(function(){
			if ($(this).is(":in-viewport")) {
				$(this).attr("src", $(this).attr("data-src"));
				$(this).addClass('inview');
			}
		});
	})


	$('.imagecontent_overlay').scroll(function() {
		$('img, video').not('.inview').each(function(){
			if ($(this).is(":in-viewport")) {
	      $(this).attr("src", $(this).attr("data-src"));
				$(this).addClass('inview');
	    }
		});
	});

	$('img, video').not('.inview').each(function(){
		if ($(this).is(":in-viewport")) {
			$(this).attr("src", $(this).attr("data-src"));
			$(this).addClass('inview');
		}
	});
}

// PLAY VIDEO IF IT IS IN VIEWPORT
function isInVi(){
		$('.image1, .image2')[0].play();


	$(document).scroll(function() {
		$('video').each(function(){
	    if ($(this).is(":in-viewport")) {
	        $(this)[0].play();
	    } else {
	        $(this)[0].pause();
	    }
		});
	});
	$('.imagecontent_overlay').scroll(function() {
		$('video').each(function(){
	    if ($(this).is(":in-viewport")) {
	        $(this)[0].play();
	    } else {
	        $(this)[0].pause();
	    }
		});
	});
};

// PLAYS VIDEO IF IT IS IN SCREEN
// function videoplaypause() {
//   var height = $(window).height();
//
//   function offscreenvideo() {
// 		console.log('öö');
//     $('.teaserwrap').each(function() {
// 			var scrollTop     = $(window).scrollTop(),
// 					elementOffset = $(this).offset().top,
// 					distance      = (elementOffset - scrollTop);
// 	      	thisheight = $(this).height();
//       if ((distance <= height) && (distance >= -thisheight)) {
//         $(this).children().children().trigger('play');
//       } else {
//         $(this).children().children().trigger('pause');
//       }
//     });
//   }
//
//   offscreenvideo();
//   $(window).resize(function() {
// 		offscreenvideo();
//   });
//
//   $(window).on('scroll mousewheel', function() {
//     offscreenvideo();
//   });
// }
